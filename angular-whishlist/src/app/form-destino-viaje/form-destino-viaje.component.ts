import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { htmlAstToRender3Ast } from '@angular/compiler/src/render3/r3_template_transform';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
    // tslint:disable-next-line:no-output-on-prefix
    @Output() onItemAdded: EventEmitter<DestinoViaje>;
    fg: FormGroup;
    minLongitud = 3;
    searchResults: string[];

  constructor(private fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();

    this.fg = this.fb.group({
       nombre: ['', Validators.compose([
         Validators.required,
         this.nombreValidatorParametrizable(this.minLongitud)
       ])],
       url: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
        console.log('cambio el formulario:', form);
    });

  }

  ngOnInit(): void {
    const elemNombre = <HTMLInputElement> document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
     .pipe(
       map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
       filter (text => text.length > 2),
       debounceTime(200),
       distinctUntilChanged(),
       switchMap(() => ajax('/assets/datos.json'))
     ).subscribe(AjaxResponse => {
      this.searchResults = AjaxResponse.response;
     });
  }


  guardar({ nombre, url }: { nombre: string; url: string; }): boolean {
 // tslint:disable-next-line:indent
  	const d = new DestinoViaje(nombre, url);
   this.onItemAdded.emit(d);
   return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return { invalidNmbre: true};
    }
    return  null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLongNombre: true};
      }
      return null;
    }
  }



}
