import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { Appstate } from '../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
 // tslint:disable-next-line:no-output-on-prefix
 @Output() onItemAdded: EventEmitter<DestinoViaje>;
 updates: string[];
 all;

 constructor(public destinosApiClient: DestinosApiClient, private store: Store<Appstate>) {
   this.onItemAdded = new EventEmitter();
   this.updates = [];
   this.store.select(state => state.destinos.favorito)
   .subscribe(d => {
     if (d != null) {
      this.updates.push('Se ha elegido a ' + d.nombre);
    }
   });
   store.select(state => state.destinos.items).subscribe(items => this.all = items);
 }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  // tslint:disable-next-line:typedef
  elegido(e: DestinoViaje) {
    this.destinosApiClient.elegir(e);
  }

  getAll() {

  }
}
