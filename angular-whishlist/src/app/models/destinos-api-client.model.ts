import { Store } from '@ngrx/store';
import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Appstate } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Injectable } from '@angular/core';
@Injectable()
export class DestinosApiClient {
  constructor(private store: Store<Appstate>) {
      }
  add(d: DestinoViaje){
    this.store.dispatch(new NuevoDestinoAction(d));
  }
  elegir(d: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}
